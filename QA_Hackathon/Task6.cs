﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QA_Hackathon
{
    class Task6
    {
        abstract class Animal_world
        {
            public string Name { get; set; }
            public int Length { get; set; }
            public int Weight { get; set; }
            public bool Has_tail { get; set; }
            public Animal_world(string name, int length, int weight, bool has_tail)
            {
                Name = name;
                Length = length;
                Weight = weight;
                Has_tail = has_tail;
            }
            public abstract void About_animals();
        }
        class Animals : Animal_world
        {
            private bool Wool { get; set; }
            private bool Claws { get; set; }
            public Animals(string name, int length, int weight, bool has_tail, bool wool, bool claws) : base(name, length, weight, has_tail)
            {
                Wool = wool;
                Claws = claws;
            }
            public override void About_animals()
            {
                Console.WriteLine($"Name of animal: {Name}\nLenght of animal: {Length}\nWeight of animal: {Weight}\nAnimal has tail: {Has_tail}\nAnimal has wool: {Wool}\nAnimal has claws: {Claws}");
            }
        }
        class Birds : Animal_world
        {
            private bool Is_flyes { get; set; }
            public Birds(string name, int length, int weight, bool has_tail, bool is_flyes) : base(name, length, weight, has_tail)
            {
                Is_flyes = is_flyes;
            }
            public override void About_animals()
            {
                Console.WriteLine($"Name of bird: {Name}\nLenght of bird: {Length}\nWeight of bird: {Weight}\nBird has tail: {Has_tail}\nBird is flying: {Is_flyes}");
            }
        }
        class Fishes : Animal_world
        {
            private bool Is_colourfull { get; set; }
            public Fishes(string name, int length, int weight, bool has_tail, bool is_colourfull) : base(name, length, weight, has_tail)
            {
                Is_colourfull = is_colourfull;
            }
            public override void About_animals()
            {
                Console.WriteLine($"Name of fish: {Name}\nLenght of fish: {Length}\nWeight of fish: {Weight}\nFish has tail: {Has_tail}\nFish is colourfull: {Is_colourfull}");
            }
        }
        class Insects : Animal_world
        {
            private bool Is_flying { get; set; }
            public Insects(string name, int length, int weight, bool has_tail, bool is_flying) : base(name, length, weight, has_tail)
            {
                Is_flying = is_flying;
            }
            public override void About_animals()
            {
                Console.WriteLine($"Name of insect: {Name}\nLenght of insect: {Length}\nWeight of insect: {Weight}\nInsect has tail: {Has_tail}\nInsect is flying: {Is_flying}");
            }
        }
    }
}
