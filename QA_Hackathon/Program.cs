﻿using System;
using System.IO;
using System.Threading;
using System.Security.Cryptography;

namespace QA_Hackathon
{
    class Program
    {
        static void Main(string[] args)
        {
            sumOfNumbers();
            recordingValuesInFile();
        }
        static private void sumOfNumbers()
        {
            Console.WriteLine("Input first number: ");
            string first_number = Console.ReadLine();
            Console.WriteLine("Input second number: ");
            string second_number = Console.ReadLine();
            int a = 0, b = 0;
            try
            {
                a = Convert.ToInt32(first_number);
                b = Convert.ToInt32(second_number);
                Console.WriteLine($"Sum of numbers: {a + b}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Unexpected exception: {e}");
            }
        }
        static private long generationOfRandomNumber(int minValue, int maxValue)
        {
            double totalSeconds = DateTime.Now.Subtract(DateTime.MinValue).TotalMilliseconds;
            Thread.Sleep(1);
            if (minValue > maxValue)
            {
                int temp1 = maxValue;
                maxValue = minValue;
                minValue = temp1;
            }
            return Convert.ToInt64(totalSeconds) % (maxValue - minValue + 1) + minValue;
        }
        static private void recordingValuesInFile()
        {
            string path = @"E:\Учёба\QA\QA_Hackathon\numbers.txt";
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                using(StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default))
                {
                    for (int i = 0; i < 10000; i++)
                    {
                        long number = generationOfRandomNumber(0, 1000);
                        sw.Write($"{number}, ");
                    }
                }
            }
        }
    }
}
